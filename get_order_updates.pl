#!/usr/bin/perl

use Getopt::Std;
use POSIX qw/ strftime /;
use strict;

my %opt;
getopts('vu:u:d:t:', \%opt);   # those with a colon following take an argument; otherwise boolean.

my @t = localtime();
my $curdate = $opt{t} || POSIX::strftime('%Y%m%d', @t);

my $updir = $opt{u} || "/home/foo/order_get";
chdir $updir;


use DBI;
my $DSN = $opt{d} ? 'dbi:mysql:dev_foo' : 'dbi:mysql:foo';
my $dbh = DBI->connect($DSN, 'foo', 'bar', { AutoCommit => 1, PrintError => 0 })
	or die "Cannot get db handle: $DBI::errstr\n";


###
### FTP connection here.
### A folder called "Reports" is where we can find the shipping details.
###      FOOInv201212182341.csv
###      FOOShip201212182341.csv
###

use Net::FTP::File;
my $ftp = Net::FTP->new("ftp.foo.com", Debug => 0)
	or die "cannot connect to ftp host: $@";
$ftp->login('foo@bar.com', "mypass")
	or die "cannot login ", $ftp->message;
$ftp->cwd('Reports')
	or die $ftp->message;

my @files = $ftp->ls()
	or die;
for(@files) {
	next unless $_ =~ /\.csv$/;
	$ftp->get($_, $_)
		or die "cannot get $_ ", $ftp->message;
	$ftp->move($_, "Archive/$_")
		or die "cannot move $_ ", $ftp->message;
}
$ftp->quit;


### inventory update:
###
### sku: foo, has inventory of 10. We sell 2 on Monday, and batch to UFSI. They give us report Monday night that says their inventory is 8.
### 8 agrees with our side, as long as we haven't sold any more in the meantime. If we have, our side will be lower.
### Say we sold 3 more on Monday night. Our level will be 5, but UFSI will say it is 8.
### So, for accurate inventory levels, we must take what they give and subtract any sold from the time of yesterday's last batch to UFSI.
###		If UFSI gets a shipment of new inventory, their level of 'foo' might then be 10-2=8 + 10 = 18.
###		We would take 18 and subtract the 3 sold on Monday night, to get 15.
###		This will hopefully be accurate, but may need to be audited occasionally.
###
###				SELECT sku,quantity FROM orderline WHERE complete <> 1;
###
###       sku might be a barcode
###
##my @invfiles = glob "FOOInv$curdate*.csv";
my @invfiles = glob "FOOInv*.csv";
for my $invfile (@invfiles) {
	open INVFILE, "< $invfile";

	my $qi = 'INSERT INTO invstatus (sku, quantity, forced, reported_on, authorized_by, comment) VALUES (?,?,?,?,?,?)';
	my $sthi = $dbh->prepare($qi);

	my $inum;
	my @ierr;
	while(<INVFILE>) {
		chomp;
		my ($sku, $qty) = split ',',$_;
		next if $sku =~ /sku/i;
		for($sku, $qty) {
			s/"//g;
		}
		#print "sku=$sku, quantity=$qty";
		$sthi->execute($sku, $qty, '1', $curdate, "UFSI", $invfile)
			or push @ierr, ( "failed for sku=$sku, quantity=$qty " . $dbh->errstr() );
		$inum++;
	}
	print "Inserted $inum inventory updates.\n" if $inum;
	print join("\n", @ierr);

	close INVFILE;
	system "mv $invfile 1archive/$invfile";
}


###
### shipping update:
###
##my @shipfiles = glob "FOOShip$curdate*.csv";
my @shipfiles = glob('FOOShip*.csv FOO*Ship.csv');
for my $shipfile (@shipfiles) {
	open SHIPFILE, "< $shipfile";

	my $qs = 'INSERT INTO shipstatus (order_number, tracking_number, cost, shipped_on) VALUES (?, ?, ?, ?)';
	my $sths = $dbh->prepare($qs);

	my $num;
	my @err;
	while(<SHIPFILE>) {
		chomp;
		my ($code, $track, $cost) = split ',',$_;
		next if $code =~ /sorder/i;
		for($code, $track, $cost) {
			s/"//g;
		}
		#print "code=$code, track=$track, cost=$cost";
		$sths->execute($code, $track, $cost, $curdate)
			or push @err, ( "failed for code=$code, track=$track, cost=$cost " . $dbh->errstr() );
		$num++;
	}
	print "Inserted $num shipments.\n" if $num;
	print join("\n", @err);

	close SHIPFILE;
	system "mv $shipfile 1archive/$shipfile";
}
