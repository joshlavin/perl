UserTag wordpress addAttr
UserTag wordpress Description	Retrieve posts from a WordPress blog; optionally, save to a database table
UserTag wordpress Documentation <<EOD

Example usage:

	<xmp>
	[wordpress
		username="user"
		password="pass"
		host="http://foo.wordpress.com"
		postid="1..2"
		rewrite-images=1
		rewrite-links=1
		rewrite-links-old="http://foo.wordpress.com/(\d\d\d\d/\d\d/[\w-]+)/"
		rewrite-links-new="/blog/${1}"
		save=blog
	]

	[comment]
	[wordpress
		username="user"
		password="pass"
		host="http://foo.com"
		commentid="39"
		rewrite-links=1
		rewrite-links-old="http://foo.wordpress.com/(\d\d\d\d/\d\d/[\w-]+)/"
		rewrite-links-new="/blog/${1}"
		save=forum
		check=blog
	]
	[/comment]
	</xmp>

	[edisplay][wdisplay]

EOD
UserTag wordpress Routine <<EOR
sub {
	my ($opt) = @_;

	use WordPress::API::Post;
	use LWP::Simple;

	use vars qw/$Tag/;
	my ($log, $die, $warn) = $Tag->logger('wp', 'logs/wordpress.log');

	my $p = WordPress::API::Post->new({
		username => $opt->{username},
		password => $opt->{password},
		proxy => $opt->{host} . '/xmlrpc.php',
	}) or return $die->('failed init');

	my $docroot = $::Variable->{DOCROOT};
	my $blog_uri = $opt->{host} . '/wp-content/uploads';

	my $save_img = sub {
		my $img = shift;
		return "/images/blog/$img" if -s "$docroot/images/blog/$img";
		my $img_data = get( "$blog_uri/$img" );
		unless($img_data) {
			$warn->("failed for image: $img");
			return "/images/blog/$img";
		}
		$Tag->write_relative_file( "images/blog/$img", { umask => 2, }, $img_data);
		$log->("saved image: images/blog/$img");
		return "/images/blog/$img";
	};

	my (@out, $tab, $db);
	if($tab = $opt->{save}) {
		$db = dbref($tab)
			or return $die->('No database table %s', $tab);
	}

	if($opt->{postid}) {

		## if this is a post
		my ($postid_beg, $postid_end) = split /\.\./, $opt->{postid};
		$postid_end ||= $postid_beg;

		for($postid_beg .. $postid_end) {

			$p->id($_)
				or return $die->('Need postid');
			$p->load;

			my $wp_hash = $p->structure_data();
			unless($wp_hash->{title}) {
				$warn->("No title for postid %s; skipping.", $wp_hash->{postid});
				next;
			}
			$wp_hash->{description} =~ s/[^\x20-\x7E\x0A\x0D]//g;  # remove high chars; gets rid of UTF-8 junk

			my $bloghostre = qr!$opt->{host}!;
			if($opt->{rewrite_images}) {
				$wp_hash->{description} =~ s{(href|src)=['"](?:$bloghostre|\.\.)?/wp-content/uploads/(\S+\.\w{2,4})['"]}{$1.'="'.$save_img->($2).'"'}ge;
			}

			if($opt->{rewrite_links}) {
				return $die->("Need 'rewrite-links-old' and 'rewrite-links-new' parameters.") unless $opt->{rewrite_links_old} and $opt->{rewrite_links_new};
				my $oldlinkre = qr!$opt->{rewrite_links_old}!;
				my $newlink = $opt->{rewrite_links_new};
				$wp_hash->{description} =~ s{href=['"]$oldlinkre['"]}{qq{qq{href="$newlink"}}}gee;
			}

			unless($tab) {
				push @out, ::uneval($wp_hash);
				next;
			}

			my %fields = (
				title       => $wp_hash->{title},
				author      => $wp_hash->{wp_author_display_name},
				slug        => $wp_hash->{wp_slug},
				tags        => $wp_hash->{mt_keywords},
				content     => $wp_hash->{description},
				post_status => $wp_hash->{post_status},
				create_date => $wp_hash->{dateCreated},
			);
			my $key = $wp_hash->{postid};
			my $status = $db->set_slice($key, \%fields)
				or return $die->("Unable to set slice with key %s", $key);
			$warn->("Saved post: '%s' to table: %s", $key, $tab) if $status;

		}

	}
	elsif($opt->{commentid}) {
		
		## if this is a comment
		my ($cmtid_beg, $cmtid_end) = split /\.\./, $opt->{commentid};
		$cmtid_end ||= $cmtid_beg;

		my ($check, $bdb);
		if($check = $opt->{check}) {
			$bdb = dbref($check)
				or return $die->("No database table '%s' to check against posts", $check);
		}

		for($cmtid_beg .. $cmtid_end) {
			my $cmt = $p->getComment($_);
			unless($cmt) {
				$warn->("No comment for commentid %s; skipping.", $_);
				next;
			}
			if($cmt->{status} ne 'approve'){
				$warn->("Commentid %s has status '%s'; skipping.", $_, $cmt->{status});
				next;
			}

			$cmt->{content} =~ s/[^\x20-\x7E\x0A\x0D]//g;  # remove high chars; gets rid of UTF-8 junk
			my $date = $Tag->convert_date({ fmt => '%F %T', body => $cmt->{date_created_gmt}, });

			if($opt->{rewrite_links}) {
				return $die->("Need 'rewrite-links-old' and 'rewrite-links-new' parameters.") unless $opt->{rewrite_links_old} and $opt->{rewrite_links_new};
				my $oldlinkre = qr!$opt->{rewrite_links_old}!;
				my $newlink = $opt->{rewrite_links_new};
				$cmt->{content} =~ s{href=['"]$oldlinkre['"]}{qq{qq{href="$newlink"}}}gee;
			}

			unless($tab) {
				push @out, ::uneval($cmt);
				next;
			}

			if($check) {
				unless( $bdb->record_exists($cmt->{post_id}) ) {
					$warn->("No post found for postid %s, commentid was: %s", $cmt->{post_id}, $_);
					next;
				}
			}

			my $artid = 'BLG' . $cmt->{post_id};
			unless( $db->record_exists($artid) ) {
				my $desc = $check ? $bdb->field($cmt->{post_id}, 'title') : $artid;
				my $record = {
					created => $Tag->convert_date( { fmt => '%Y%m%d%H%M' }),
					artid => $artid,
					subject => "Comment on $desc",
					username => "interch",
					name => "",
					email => '',
					score => 1,
					comment => "",
					parent => 0,
				};
				$db->set_slice($artid, $record);
			}

			my $exists;
			if( $exists = $db->foreign($_, 'extended') ) {
				$warn->("Post %s already done; forum id: %s", $_, $exists);
				next;
			}

			my %fields = (
				parent   => $artid,
				artid    => $artid,
				created  => $date,
				email    => $cmt->{author_email},
				name     => $cmt->{author},
				host     => $cmt->{author_ip},
				score    => 1,
				subject  => $cmt->{author_url},
				comment  => $cmt->{content},
				extended => $_,
			);
			my $status = $db->set_slice(undef, \%fields)
				or return $die->("Unable to set slice for commentid %s", $_);
			$warn->("Saved comment: '%s' to table: %s. Forum id: %s", $_, $tab, $status) if $status;
		}

	}
	else {
		return $die->('Need postid or commentid.');
	}

	return join "\n", @out;

## 	+-------------+--------------+------+-----+---------+----------------+
## 	| Field       | Type         | Null | Key | Default | Extra          |
## 	+-------------+--------------+------+-----+---------+----------------+
## 	| code        | int(11)      | NO   | PRI | NULL    | auto_increment | 
## 	| title       | varchar(128) | NO   |     | NULL    |                | 
## 	| author      | varchar(128) | YES  |     | NULL    |                | 
## 	| slug        | varchar(128) | YES  | MUL | NULL    |                | 
## 	| tags        | varchar(255) | NO   | MUL | NULL    |                | 
## 	| content     | text         | NO   |     | NULL    |                | 
## 	| post_status | varchar(16)  | YES  |     | NULL    |                | 
## 	| create_date | varchar(32)  | NO   | MUL |         |                | 
## 	+-------------+--------------+------+-----+---------+----------------+

}
EOR
