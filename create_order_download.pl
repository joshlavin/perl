#!/usr/bin/perl

use Getopt::Std;
use POSIX qw/ strftime /;
use strict;

my %opt;

my @t = localtime();
my $curdate = POSIX::strftime('%Y%m%d', @t);
my $curhour = POSIX::strftime('%H', @t);
getopts('vda:u:s:e:p:', \%opt);
##
## opts:
##

my $verbose = $opt{v} ? '-v' : '';

my $ardir = $opt{a} || "/home/foo/order_send";

my $start_order	= $opt{s} || '';
my $end_order	= $opt{e} || '';

my $promo = $opt{p} || 'INS-0002-0714';

my $filename = "FOOord". POSIX::strftime('%m%d%Y', @t) .'.csv';
my $filename_time = "FOOord". POSIX::strftime('%Y%m%d_%H%M%S', @t) .'.csv';
my $ufn = "$ardir/$filename";

####################################
##
## CarrierCode is either UPS Ground Residential or SurePost (logic below), or could even be UNPP for overnight
## may have to alter ItemNumber (might be a barcode)
##
####################################

## file columns and order:
my @cols = qw/
	FileType
	ClientCode
	OrderNumber
	CarrierCode
	ShipToName
	ShipToAddr1
	ShipToAddr2
	ShipToAddr3
	ShipToAddr4
	ShipToCity
	ShipToState
	ShipToPostal
	ShipToCountry
	ItemNumber
	QtyOrdered
	ShipToPhone
	EmailName
	EmailAddress
	OrderDate
	ExpectedShipDate
	SoldToName
	SoldToAddr1
	SoldToAddr2
	SoldToAddr3
	SoldToAddr4
	SoldToCity
	SoldToState
	SoldToPostal
	SoldToCountry
	SoldToPhone
	SubTotal
	Tax
	ShippingCharge
	Discount%
	DiscountAmount
	Total
	Tender1Description
	Tender1Amount
	CODInvoiceAmount
	DeclaredValue
	ShipToNote1
	ShipToNote2
	OrderComments
	ItemPrice
	LineComments
	InsureValue
/;

## hard-coded or default fields:
my %hard = qw/
	FileType       STANDARD
	ClientCode     FOO
/;

## remap transactions field on right to field on left (these are the fields we will get from transactions):
my %tmap = qw/
	OrderNumber     order_number
	ShipToName      fname::lname
	ShipToAddr1     address1
	ShipToAddr2     address2
	ShipToAddr3     company
	ShipToCity      city
	ShipToState     state
	ShipToPostal    zip
	ShipToCountry   country
	ShipToPhone     phone_day
	EmailAddress    email
	OrderDate       my_order_ymd
	SoldToName      b_fname::b_lname
	SoldToAddr1     b_address1
	SoldToAddr2     b_address2
	SoldToCity      b_city
	SoldToState     b_state
	SoldToPostal    b_zip
	SoldToCountry   b_country
	Tax             salestax
	ShippingCharge  shipping
	Total           total_cost
	OrderComments   comments
/;

## remap orderline field on right to field on left (these are the fields we will get from orderline): 
my %omap = qw/
	ItemNumber    sku
	QtyOrdered    quantity
	SubTotal      subtotal
	ItemPrice     price
/;

## limit field length:
my %ll = qw/
	FileType            8
	ClientCode          3
	OrderNumber         17
	CarrierCode         15
	ShipToName          30
	ShipToAddr1         30
	ShipToAddr2         30
	ShipToAddr3         30
	ShipToAddr4         30
	ShipToCity          30
	ShipToState         10
	ShipToPostal        10
	ShipToCountry       5
	ItemNumber          15
	ShipToPhone         20
	EmailName           30
	EmailAddress        256
	SoldToName          30
	SoldToAddr1         30
	SoldToAddr2         30
	SoldToAddr3         30
	SoldToAddr4         30
	SoldToCity          30
	SoldToState         10
	SoldToPostal        10
	SoldToCountry       5
	SoldToPhone         20
	Tender1Description  256
	ShipToNote1         50
	ShipToNote2         50
	OrderComments       90
	LineComments        90
/;

## non-CONUS states (get SurePost shipping):
my %nonconus = qw/
	AK   1
	AS   1
	FM   1
	GU   1
	HI   1
	MH   1
	MP   1
	PR   1
	PW   1
	VI   1
	AA   1
	AE   1
	AP   1
/;

use DBI;
my $DSN = $opt{d} ? 'dbi:mysql:dev_foo' : 'dbi:mysql:foo';
my $dbh = DBI->connect($DSN, 'foo', 'bar', { AutoCommit => 1, PrintError => 0 })
	or die "Cannot get db handle: $DBI::errstr\n";

my $q1;
if($start_order) {
	$q1 = qq{
		SELECT *
		FROM transactions
		WHERE ( complete <> 1 OR complete IS NULL )
		AND status = 'pending'
		AND ( submitted_for_payment = 1 OR submitted_for_payment = '' )
		AND code >= '$start_order'
	};
	if($end_order) {
		$q1 .= " AND code <= '$end_order'";
	}
	else {
		$q1 .= " AND order_ymd < '$curdate'";
	}
	warn "query = $q1" if $verbose;
}
else {
	my $hour = $curhour - 1;  ## give nearly 2 hours of queue time for any order changes, before batching (we run this at :55, so: 7(:55) - 1 = 6:00. 1:55 from 7:55)
	$q1 = qq{
		SELECT *
		FROM transactions
		WHERE ( complete <> 1 OR complete IS NULL )
		AND status = 'pending'
		AND ( submitted_for_payment = 1 OR submitted_for_payment = '' )
		AND order_date <= '$curdate $hour:00:00'
	};
}

my $q3 = 'SELECT * FROM orderline WHERE order_number = ?';
my $q4 = "UPDATE transactions SET complete = 1, status = 'batched' WHERE code = ?";
my $q5 = 'SELECT cost FROM products WHERE sku = ?';
my $q6 = "SELECT i.quantity, m.timed_promotion, m.start_date, m.finish_date FROM inventory i LEFT JOIN merchandising m ON i.sku=m.sku WHERE i.sku = '$promo'";
my $q7 = 'INSERT INTO invstatus (sku, quantity, reported_on, authorized_by) VALUES (?,?,?,?)';

my $sth1 = $dbh->prepare($q1)
	or die "Cannot prepare $q1: " . $dbh->errstr;

my $sth3 = $dbh->prepare($q3)
	or die "Cannot prepare $q3: " . $dbh->errstr;

my $sth4 = $dbh->prepare($q4)
	or die "Cannot prepare $q4: " . $dbh->errstr;

my $sth5 = $dbh->prepare($q5)
	or die "Cannot prepare $q5: " . $dbh->errstr;

my $sth6 = $dbh->prepare($q6)
	or die "Cannot prepare $q6: " . $dbh->errstr;

my $sth7 = $dbh->prepare($q7)
	or die "Cannot prepare $q7: " . $dbh->errstr;

my $do_promo;
PROMO: {
	last PROMO unless $promo;
	$sth6->execute()
		or die "Cannot execute $q6: " . $dbh->errstr;
	while(my $prec = $sth6->fetchrow_hashref()) {
		my $qty    = $prec->{quantity};
		my $timed  = $prec->{timed_promotion};
		my $start  = $prec->{start_date};
		my $finish = $prec->{finish_date};
		last PROMO unless $qty > 0;
		if($timed) {
			last PROMO if $start > $curdate;
			last PROMO if $finish < $curdate;
		}
		$do_promo = 1;
#use Data::Dumper;
#print Dumper("promo:$promo, qty:$qty, timed:$timed, start:$start, finish:$finish, do_promo=$do_promo");
#die;
	}
}

$sth1->execute()
	or die "Cannot execute $q1: " . $dbh->errstr;

sub discon {
	undef $sth1;
	undef $sth3;
	undef $sth4;
	undef $sth5;
	undef $sth6;
	undef $sth7;
	$dbh->disconnect;
}

my ($did, $num);
while(my $trec = $sth1->fetchrow_hashref()) {
	my $on = $trec->{code};
	my %out;

	my $total_cogs;

	## field formatting:
	my ($y, $m, $d) = unpack( 'a4 a2 a2', $trec->{order_ymd});
	$trec->{my_order_ymd} = "$m/$d/$y";
	##

	while(my ($fn, $v) = each %hard ) {   # hard-coded or default fields
		$out{$fn} = $v;
	}
	while(my ($fn, $v) = each %tmap ) {   # transaction fields
		if ($v eq 'order_number') {
			if ( $trec->{payment_method} =~ /^PO number/ ) {
				$v = 'po_number';
			}
			if ($trec->{wholesale}) {
				my $wholesale_on;
				($wholesale_on = $on) =~ s/^AN/W/;
				($wholesale_on = $on) =~ s/^TEST/W/ if $opt{d};
				$trec->{order_number} = $wholesale_on;
			}
		}
		my @vals = split /::/, $v;
		for(@vals) {
			$_ = $trec->{$_};
		}
		$out{$fn} = join ' ', grep {$_} @vals;   # uses grep to remove empty elements
	}

	my @olines;
	$sth3->execute($on)
		or die "Cannot fetch orderline records for '$on': " . $sth3->errstr;
	while( my $orec = $sth3->fetchrow_hashref ) {
		if(-e $ufn) {
			open XMIT, ">> $ufn"
				or die "Unable to open existing order update files: $!\n";
		}
		unless ($did or -e $ufn) {
			## print column headers
			open XMIT, "> $ufn"
				or die "Unable to open order update files: $!\n";
			print XMIT '"';   # first field begin quote
			print XMIT join('","', @cols);
			print XMIT '"';   # last field end quote
			print XMIT "\n";
		}
		$did++;

		## can't print orderline rows to XMIT right here, because we have to do other calculations below (CarrierCode, etc), so save temporarily to @olines.
		my %tmp;
		while(my ($fn, $v) = each %omap ) {   # orderline fields
			$tmp{$fn} = $orec->{$v};
		}
		push @olines, \%tmp;

		## get COGS
		$sth5->execute($orec->{sku});
		my $cogs = $sth5->fetchrow || 0;
		my $qty = $orec->{quantity} || 1;
		$total_cogs += ($cogs * $qty);
	}

	## decide on CarrierCode:
	my $surepost;
	if($trec->{country} ne 'US') {
		$surepost = 1;
	}
	elsif( $nonconus{$trec->{state}} ) {
		$surepost = 1;
	}
	elsif(
		$trec->{address1} =~ /\bp\.?o\.? *box/i
		or
		$trec->{address1} =~ /^ *box *\d/i
		or
		$trec->{address1} =~ /post +off(ice)? *box *\d/i
	) {
		$surepost = 1;
	}
	elsif($total_cogs < 10) {
		$surepost = 1;
	}
	$out{CarrierCode} = $surepost ? 'SPPS' : 'UGR';
	$out{CarrierCode} = '3PTY' if $out{OrderNumber} !~ /^(W|AN)/;   # set for PO
	$out{CarrierCode} = 'UNPP' if $out{OrderComments} =~ /overnight/i;
	## end CarrierCode

	if($do_promo) {
		my %pp = (
			sku => $promo,
			quantity => 1,
			subtotal => '0.00',
			price => '0.00',
		);
		my %ptmp;
		while(my ($fn, $v) = each %omap ) {   # orderline fields
			$ptmp{$fn} = $pp{$v};
		}
		push @olines, \%ptmp;
		$sth7->execute($promo, -1, $curdate, "create_order_download.pl - $on");
	}

	while(my ($k, $v) = each %out ) {   # length limits
		$v =~ s|"|""|g;   # if any quotes in the text, prepend another quote to them
		$ll{$k} ||= 256;
		$v = substr($v, 0, $ll{$k}) if length($v) > $ll{$k};
		$out{$k} = $v;
	}

#use Data::Dumper;
#print Dumper(\%out);
#print "\n\n";
#print Dumper(@olines);
#last;

	for my $o (@olines) {
		while(my ($fn, $v) = each %$o ) {
			$out{$fn} = $v;
		}
		for(my $i=0; $i<scalar(@cols); $i++) {
			print XMIT ',' if $i;
			my $v = $out{$cols[$i]};
			next unless defined $v;
			print XMIT '"' . $v . '"';
		}
		print XMIT "\n";
	}

	$num++;
	##
	next if $opt{d};
	##
	$sth4->execute($on)
			or die "Unable to set complete field to 1: " . $sth4->errstr;
}

if(not $did) {
	warn "No orders to transmit.\n" if $verbose;
}
else {
	close XMIT;
	print "Did $num orders.\n";
}

discon();


###
### need to FTP to their server.
### copy file to a backup directory, with time appended, so we don't overwrite for multiple files per day
###

die 'done, no ftp' if $opt{d};

for(glob "$ardir/*.csv") {

	use Net::FTP;
	my $ftp = Net::FTP->new("ftp.foo.com", Debug => 0)
		or die "cannot connect to ftp host: $@";
	$ftp->login('FOO@bar.com', "mypass")
		or die "cannot login ", $ftp->message;
	$ftp->cwd("SO_Files");
	$ftp->put($_)
		or die "cannot put $_ ", $ftp->message;
	$ftp->quit;

	my $time = POSIX::strftime('%H%M%S', @t);
	my $file = $_;
	$file =~ s/^.*\///;  # get rid of dir
	my $filetime = $file;
	$filetime =~ s/(\d\d)(\d\d)(\d\d\d\d)/$3$1$2/;  # reorder date
	$filetime =~ s/\.csv/_$time.csv/;

	system "mv $ardir/$file $ardir/1archive/$filetime";

}
